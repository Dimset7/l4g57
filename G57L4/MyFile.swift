//
//  MyFile.swift
//  G57L4
//
//  Created by student on 10/5/17.
//  Copyright © 2017 student. All rights reserved.
//

import UIKit

class MyFile: NSObject {
    
    
    static func printTest() {
        print("Hello world")
    }
    
    static func countHello(count: Int) {
        for _ in 0..<count {
            print("Hello world")
        }
    }
}
